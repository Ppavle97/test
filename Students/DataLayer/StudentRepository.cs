﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StudentRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog = BazaPodataka; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Student> GetAllStudents()
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Students";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<Student> listToReturn = new List<Student>();

                while (sqlDataReader.Read())
                {
                    Student s = new Student();
                    s.Id = sqlDataReader.GetInt32(0);
                    s.Name = sqlDataReader.GetString(1);
                    s.Surname = sqlDataReader.GetString(2);
                    s.Age = sqlDataReader.GetInt32(3);
                    s.IndexNumber = sqlDataReader.GetString(4);
                    listToReturn.Add(s);
                }

                return listToReturn;
            }
        }

        public int InsertStudent(Student s)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "INSERT INTO Students VALUES(" + string.Format(
                    "'{0}','{1}',{2},'{3}'", s.Name, s.Surname, s.Age, s.IndexNumber) + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int UpdateStudent(Student s)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "UPDATE Students SET Name = '" + s.Name + 
                    "', Surname = '" + s.Surname + "', Age = " + s.Age + 
                    ", IndexNumber = '" + s.IndexNumber + "' WHERE Id = " + s.Id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteStudent(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "DELETE FROM Students WHERE Id = " + id;

                return sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
