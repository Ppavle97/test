﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StudentBusiness
    {
        private StudentRepository studentRepository;

        public StudentBusiness()
        {
            this.studentRepository = new StudentRepository();
        }

        public List<Student> GetAllStudents()
        {
            return this.studentRepository.GetAllStudents();
        }

        public List<Student> GetStudentsByYear(string year)
        {
            return this.studentRepository.GetAllStudents().Where(s => s.IndexNumber.Contains(year)).ToList();
        }

        public bool InsertStudent(Student s)
        {
            int result = 0;
            if(s != null)
            {
                result = this.studentRepository.InsertStudent(s);
            }
            if(result > 0)
            {
                return true;
            }
            return false;
        }

        public bool UpdateStudent(Student s)
        {
            int result = 0;
            if (s != null)
            {
                result = this.studentRepository.UpdateStudent(s);
            }
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteStudent(int id)
        {
            int result = this.studentRepository.DeleteStudent(id);
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}
