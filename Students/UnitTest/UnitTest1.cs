﻿using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        public Student student = new Student();
        public StudentRepository studentRepository = new StudentRepository();
        public int studentId;

        [TestInitialize]
        public void InitTest()
        {
            student.Name = "Test";
            student.Surname = "TestSurname";
            student.Age = 20;
            student.IndexNumber = "34/2016";

            studentRepository.InsertStudent(student);
        }

        [TestMethod]
        public void TestiranjeUnosaStudenata()
        {
            int counter = 0;
            List<Student> studenti = new List<Student>();
            studenti = studentRepository.GetAllStudents();
            foreach (Student s in studenti)
            {
                if (s.Name == student.Name)
                {
                    counter++;
                    studentId = s.Id;
                }
            }

            Assert.AreNotEqual(0, counter);
        }

        [TestMethod]
        public void TestiranjeAzuriranjaStudenta()
        {
            var counter = 0;
            Student prevStudent = student;

            var studenti = studentRepository.GetAllStudents();
            foreach (Student s in studenti)
            {
                if (s.Name.Equals(student.Name))
                {
                    counter++;
                    student.Id = s.Id;
                }
            }

            student.Name = "NijeTest";
            studentRepository.UpdateStudent(student);
            foreach (Student s in studenti)
            {
                if (s.Name.Equals("NijeTest"))
                {
                    counter++;
                    student.Id = s.Id;
                }
            }

            Assert.AreNotEqual(0, counter);
        }

        [TestMethod]
        public void TestiranjeAzuriranjaStudenta2()
        {
            int counter = 0;
            student.Name = "NovoIme";
            studentRepository.UpdateStudent(student);
            var studenti = studentRepository.GetAllStudents();
            foreach(Student s in studenti)
            {
                if (student.Name.Equals(s.Name))
                {
                    counter++;
                    student.Id = s.Id;
                }  
            }

            Assert.AreNotEqual(0, counter);
        }

        [TestCleanup]
        public void Cleanup()
        {
            studentRepository.DeleteStudent(student.Id);
        }
    }
}
